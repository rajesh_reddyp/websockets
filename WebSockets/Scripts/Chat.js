﻿Chat = function () {
    //var socket = new WebSocket("ws://echo.websocket.org");
    var socket = new WebSocket("ws://localhost:8181");
    
    socket.onopen = function (event) {
        alert('connection established');
        var label = document.getElementById("status-label");
        label.innerHTML = 'connection established !';
    }
    socket.onmessage = function (event) {
        console.log(" Data received");
        if (typeof event.data === "string")
        {
            var label = document.getElementById("status-label");
            label.innerHTML += "<br/>"+event.data;
        }
    }

    socket.onclose = function (event) {
        alert('Connection closed');
        var code = event.code;
        var reason = event.reason;
        var wasClean = event.wasClean;
        var label = document.getElementById("status-label");
        if (wasClean)
            label.innerHTML = 'Connection closed normally';
        else
            label.innerHTML = 'Connection closed with message ' + reason + '(Code:' + code + ')';
    }
    socket.onerror = function (event) {
        console.log("Error occurred.");
        // Inform the user about the error.
        var label = document.getElementById("status-label");
        label.innerHTML = "Error: " + event;
    }
    var textView = document.getElementById('text-view');
    var buttonSend = document.getElementById('send-button');
    var buttonStop = document.getElementById('stop-button');
    buttonSend.onclick = function () {
        if(socket.readyState==WebSocket.OPEN)
        socket.send(textView.value);
    }
    buttonStop.onclick = function () {
        // Close the connection, if open.
        if (socket.readyState === WebSocket.OPEN) {
            socket.close();
        }
    }
}