﻿using Fleck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSocket
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new WebSocketServer("ws://localhost:8181");
            var clients = new List<IWebSocketConnection>();
            server.Start((socket) =>{
                socket.OnOpen = () =>
                {
                    clients.Add(socket);
                };
                socket.OnClose = () => {
                    clients.Remove(socket);
                };

                socket.OnMessage=(message)=>{
                    foreach (var client in clients)
                        client.Send(socket.ConnectionInfo.Id+ " says : " + message);
                };
            });
            Console.ReadLine();
        }
    }
}
